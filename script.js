/**
 * Created by Jan on 08.03.16.
 */

var version = "1.0.0";

//--------------
// Game
//--------------

new function () {

    function TicTacToe (element) {
        var current = 0, players = [ "x", "o" ], finished;

        function check () {
            var i, tds, winner,
                full = true;

            if (!finished) {

                tds = element.getElementsByTagName("td");

                // alle Felder markiert?
                for (i = 0; i < tds.length; i++) {
                    if (tds[i].className === "") {
                        full = false;
                    }
                }

                for (i = 0; i < 3; i++) {

                    // senkrecht
                    if (tds[0 + i].className !== ""
                        && tds[0 + i].className === tds[3 + i].className
                        && tds[3 + i].className === tds[6 + i].className
                        ) {
                        // we have a winner!
                        winner = tds[0 + i].className;
                    }

                    // waagrecht
                    if (tds[i*3 + 0].className !== ""
                        && tds[i*3 + 0].className === tds[i*3 + 1].className
                        && tds[i*3 + 1].className === tds[i*3 + 2].className
                        ) {
                        // we have a winner!
                        winner = tds[i*3].className;
                    }
                }

                // diagonal links oben nach rechts unten
                if (tds[0].className === tds[4].className
                    && tds[4].className === tds[8].className
                    ) {
                    winner = tds[0].className;
                }

                // diagonal rechts oben nach links unten
                if (tds[2].className === tds[4].className
                    && tds[4].className === tds[6].className
                    ) {
                    winner = tds[2].className;
                }

                // game over?
                if (full || winner) {
                    finished = true;
                    element.className += " game-over";

                    if (winner) {
                        element.className += " " + winner;
                    }
                }
            }
        }

        // click / tap verarbeiten
        function mark (event) {
            // Tabellenzelle bestimmen
            var td = event.target;

            // Zelle bei Bedarf markieren
            if (!finished && td.className.length < 1) {
                td.className = players[current]; // Klassennamen vergeben
                current = 1 - current; // zwischen 0 und 1 hin- und herschalten
                check(); // Spiel zuende?
            }
        }

        // Ereignis bei Tabelle überwachen
        element.addEventListener("click", mark);
    }

    // finde alle Spielfeld-Tabellen
    var games = document.querySelectorAll("table.tic-tac-toe");

    for (var i = 0; i < games.length; i++) {
        TicTacToe(games[i]); // aktuelles Fundstück steht in games[i]
    }
};